package ntnu.idatt2001.oahjellj.mappe2.hospital.model;

import java.util.Objects;

/**
 * Class representing a patient
 *
 * @author OAHjellj
 */
public class Patient {
    private String firstName;
    private String lastName;
    private String generalPractitioner;
    private String diagnosis;
    //user should not be allowed to change social security number
    private final String socialSecurityNumber;

    /**
     * Constructor for the patient
     *
     * @param firstName First name
     * @param lastName Last name
     * @param generalPractitioner Name of primary doctor
     * @param socialSecurityNumber 11 digit number string
     */
    public Patient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber) {
        Objects.requireNonNull(firstName, "firstName cannot be null");
        Objects.requireNonNull(lastName, "lastName cannot be null");
        Objects.requireNonNull(generalPractitioner, "generalPractitioner cannot be null");
        Objects.requireNonNull(socialSecurityNumber, "socialSecurityNumber cannot be null");
        if (socialSecurityNumber.length() != 11) throw new IllegalArgumentException("socialSecurityNumber is wrong length");
        if (socialSecurityNumber.replaceAll("[^\\d.]", "").length() != socialSecurityNumber.length())
            throw new IllegalArgumentException("socialSecurityNumber contains non numbers");

        this.firstName = firstName;
        this.lastName = lastName;
        this.generalPractitioner = generalPractitioner;
        this.diagnosis = "";
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Gets the firstName
     *
     * @return firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the lastName
     *
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the generalPractitioner
     *
     * @return generalPractitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets the generalPractitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Gets the diagnosis
     *
     * @return diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets the diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Gets the socialSecurityNumber
     *
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Checks if the object is equal to this patient
     *
     * @param obj :object
     * @return true if equal to this based on social security number
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Patient)) return false;
        Patient patientObj = (Patient) obj;
        return socialSecurityNumber.equals(patientObj.getSocialSecurityNumber());
    }

    /**
     * Makes a HashCode based on the social security number of the patient
     *
     * @return hashCode : int
     */
    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
