package ntnu.idatt2001.oahjellj.mappe2.hospital.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ntnu.idatt2001.oahjellj.mappe2.hospital.model.Patient;

import java.util.Optional;

/**
 * Controller class for add and edit dialog
 */
public class ManagePatientController {
    @FXML private TextField firstNameTextField;
    @FXML private TextField lastNameTextField;

    @FXML private Label socialSecurityLabel;
    @FXML private TextField socialSecurityNumberTextField;

    @FXML private Label diagnosisLabel;
    @FXML private TextField diagnosisTextField;

    @FXML private TextField generalPractitionerTextField;

    @FXML private Button saveButton;
    @FXML private Button cancelButton;

    private Patient patient;

    /**
     * Gets the new or edited patient
     *
     * @return Optional patient
     */
    public Optional<Patient> getPatient() {
        return (this.patient == null)
                ? Optional.empty()
                : Optional.of(this.patient);
    }

    /**
     * Initializes the controller
     */
    @FXML
    public void initialize() { }

    /**
     * Sets the patient to the given patient and updates the text fields
     *
     * @param patient to be edited
     */
    public void setPatientToEdit(Patient patient) {
        firstNameTextField.setText(patient.getFirstName());
        lastNameTextField.setText(patient.getLastName());
        socialSecurityNumberTextField.setText(patient.getSocialSecurityNumber());
        generalPractitionerTextField.setText(patient.getGeneralPractitioner());
        diagnosisTextField.setText(patient.getDiagnosis());

        //social security number cannot be edited
        socialSecurityLabel.setVisible(false);
        socialSecurityNumberTextField.setVisible(false);

        //Diagnosis can only be set when editing
        diagnosisLabel.setVisible(true);
        diagnosisTextField.setVisible(true);

        this.patient = patient;
    }

    /**
     * OnAction method for the save button
     *
     * @param actionEvent ActionEvent
     */
    public void onSaveButtonClicked(ActionEvent actionEvent) {
        if (!hasLegalName()) return;
        if (!hasLegalSocial()) return;

        if (patient == null) {
            this.patient = new Patient(firstNameTextField.getText().trim(), lastNameTextField.getText().trim(), generalPractitionerTextField.getText().trim(), socialSecurityNumberTextField.getText().trim());
        }else {
            this.patient.setFirstName(firstNameTextField.getText().trim());
            this.patient.setLastName(lastNameTextField.getText().trim());
            this.patient.setDiagnosis(diagnosisTextField.getText().trim());
            this.patient.setGeneralPractitioner(generalPractitionerTextField.getText().trim());
        }

        closeStage(actionEvent);
    }

    /**
     * Checks if the socialSecurityNumber is a legal format
     *
     * @return true if legal
     */
    private boolean hasLegalSocial() {
        String testSocialSecurityNumber = socialSecurityNumberTextField.getText()
            .trim()
            .replaceAll("[^\\d.]", "");

        if (testSocialSecurityNumber.length() != 11) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Illegal Social Security Number");
            alert.setHeaderText("Social security number must be 11 digits and numbers only");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    /**
     * Checks if the name is a legal format
     *
     * @return true if legal
     */
    private boolean hasLegalName() {
        if (firstNameTextField.getText().trim().isEmpty() || lastNameTextField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Illegal Name");
            alert.setHeaderText("Name must contain at least one first and last name");
            alert.showAndWait();
            return false;
        }
        return true;
    }

    /**
     * On action method for the cancel button
     *
     * @param actionEvent ActionEvent
     */
    public void onCancelButtonClicked(ActionEvent actionEvent) {
        closeStage(actionEvent);
    }

    /**
     * Closes the stage
     *
     * @param actionEvent ActionEvent
     */
    private void closeStage(ActionEvent actionEvent) {
        Node source = (Node)  actionEvent.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }
}
