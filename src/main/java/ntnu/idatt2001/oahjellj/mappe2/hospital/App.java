package ntnu.idatt2001.oahjellj.mappe2.hospital;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

/**
 * Main class for the application
 *
 * @author OAHjellj
 */
public class App extends Application {
    private static final String TITLE = "Clinic Registry";
    private static final Image LOGO = new Image(Objects.requireNonNull(App.class.getResource("icon/logo.png")).toExternalForm());

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/HospitalRegistryView.fxml"));
        Parent root = fxmlLoader.load();

        Scene scene = SceneFactory.create(root);
        primaryStage.setTitle(TITLE);
        primaryStage.getIcons().add(LOGO);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
