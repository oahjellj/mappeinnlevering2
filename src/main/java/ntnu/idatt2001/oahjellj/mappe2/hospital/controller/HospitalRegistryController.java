package ntnu.idatt2001.oahjellj.mappe2.hospital.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ntnu.idatt2001.oahjellj.mappe2.hospital.App;
import ntnu.idatt2001.oahjellj.mappe2.hospital.model.FileManager;
import ntnu.idatt2001.oahjellj.mappe2.hospital.model.Patient;
import ntnu.idatt2001.oahjellj.mappe2.hospital.model.PatientRegistry;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * Controller for HospitalRegistryView
 *
 * @author OAHjellj
 */
public class HospitalRegistryController {
    @FXML private Menu helpMenu;
    @FXML private Menu editMenu;
    @FXML private Menu fileMenu;

    @FXML private Button addPatientButton;
    @FXML private Button removePatientButton;
    @FXML private Button editPatientButton;

    @FXML private TableView<Patient> patientTable;
    @FXML private TableColumn<Patient, String> firstNameTableColumn;
    @FXML private TableColumn<Patient, String> lastNameTableColumn;
    @FXML private TableColumn<Patient, String> doctorTableColumn;
    @FXML private TableColumn<Patient, String> diagnosisTableColumn;
    @FXML private TableColumn<Patient, String> socialSecurityNumberTableColumn;

    @FXML private Label statusLabel;

    private final Image logo = new Image(Objects.requireNonNull(App.class.getResource("icon/logo.png")).toExternalForm());
    private final Image add = new Image(Objects.requireNonNull(App.class.getResource("icon/addPerson.png")).toExternalForm());
    private final Image edit = new Image(Objects.requireNonNull(App.class.getResource("icon/editPerson.png")).toExternalForm());
    private final Image remove = new Image(Objects.requireNonNull(App.class.getResource("icon/removePerson.png")).toExternalForm());
    private PatientRegistry patientRegistry;

    /**
     * Initializes the view and assigns valueFactories to the columns
     */
    @FXML
    private void initialize() {
        firstNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        doctorTableColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        diagnosisTableColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        socialSecurityNumberTableColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        patientRegistry = new PatientRegistry();
    }

    /**
     * Opens the add a patient view
     *
     * @param actionEvent ActionEvent
     * @throws IOException if the view is not found
     */
    @FXML
    private void onAddPatient(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/ManagePatientView.fxml"));
        Parent parent = fxmlLoader.load();
        ManagePatientController managePatientController = fxmlLoader.getController();

        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(add);
        stage.setScene(scene);
        stage.showAndWait();

        if(managePatientController.getPatient().isPresent()) {
            Patient patient = managePatientController.getPatient().get();
            if (!patientRegistry.add(patient)) {
                statusLabel.setText("Patient already exist");
                return;
            }
            patientTable.getItems().add(patient);
            patientTable.refresh();
            statusLabel.setText("Patient added");
            return;
        }
        statusLabel.setText("Add patient process interrupted");
    }

    /**
     * Removes the selected patient from the directory
     *
     * @param actionEvent ActionEvent
     */
    @FXML
    private void onRemovePatient(ActionEvent actionEvent) {
        Patient patient = patientTable.getSelectionModel().getSelectedItem();

        if(patient == null) {
            patientNotFoundAlert();
            return;
        }

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Remove Patient?");
        addIconToAlert(alert, remove);
        alert.setHeaderText(null);
        alert.setContentText("Is it ok to remove " + patient.getFirstName() + " " + patient.getLastName() + "?");

        Optional<ButtonType> resultBoolean = alert.showAndWait();
        if (resultBoolean.isPresent() && resultBoolean.get() == ButtonType.OK && patientRegistry.remove(patient)){
            patientTable.getItems().remove(patient);
            patientTable.refresh();
            statusLabel.setText("Patient removed");
            return;
        }

        statusLabel.setText("Remove process interrupted");
    }

    /**
     * Opens the window to edit the selected patient
     *
     * @param actionEvent ActionEvent
     * @throws IOException if view is not found
     */
    @FXML
    private void onEditPatient(ActionEvent actionEvent) throws IOException {
        Patient patient = patientTable.getSelectionModel().getSelectedItem();

        if(patient == null) {
            patientNotFoundAlert();
            return;
        }

        Patient patientCopy = new Patient(patient.getFirstName(),patient.getLastName(),patient.getGeneralPractitioner(),patient.getSocialSecurityNumber());
        patientCopy.setDiagnosis(patient.getDiagnosis());

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/ManagePatientView.fxml"));
        Parent parent = fxmlLoader.load();
        ManagePatientController managePatientController = fxmlLoader.getController();

        managePatientController.setPatientToEdit(patient);

        Scene scene = new Scene(parent);
        Stage stage = new Stage();
        stage.getIcons().add(edit);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();

        if (allFieldsEqual(patient, patientCopy)) {
            statusLabel.setText("Edit process was interrupted");
            return;
        }
        patientTable.refresh();
        statusLabel.setText("Patient edited");
    }

    /**
     * Checks if all the fields of patient 1 and 2 are all equal
     *
     * @param patient1 patient
     * @param patient2 patient
     * @return true if there are no differences
     */
    private boolean allFieldsEqual(Patient patient1, Patient patient2) {
        if (!patient1.getFirstName().equals(patient2.getFirstName())) return false;
        if (!patient1.getLastName().equals(patient2.getLastName())) return false;
        if (!patient1.getGeneralPractitioner().equals(patient2.getGeneralPractitioner())) return false;
        if (!patient1.getDiagnosis().equals(patient2.getDiagnosis())) return false;
        return patient1.equals(patient2);
    }

    /**
     * Closes the menus in the menuBar
     *
     * @param actionEvent ActionEvent
     */
    @FXML
    private void onExit(ActionEvent actionEvent) { }

    /**
     * Opens the fileChooser so the user can choose a to import from, reads the file and adds all the patients to the table
     *
     * @param actionEvent ActionEvent
     * @throws IOException if file is not the correct type
     */
    @FXML
    private void onImport(ActionEvent actionEvent) throws IOException {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(new Stage());

        if (file == null) {
            statusLabel.setText("Import fail");
            return;
        }
        if (!isCSV(file.getName())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Wrong file type!");
                alert.setHeaderText("File is not csv type");
                addIconToAlert(alert, logo);
                alert.showAndWait();

                statusLabel.setText("Import fail");
                return;
        }

        ArrayList<Patient> patientList = FileManager.importFromFile(file);
        patientRegistry.addAll(patientList);
        patientTable.getItems().addAll(patientRegistry.getPatients());
        patientTable.refresh();
        statusLabel.setText("Import successful");
    }

    /**
     * Checks if file name ends with .csv
     *
     * @param fileName to be checked
     * @return true if file is csv type
     */
    private boolean isCSV(String fileName) {
        String[] strings = fileName.split("\\.");
        return strings[strings.length - 1].equals("csv");
    }

    /**
     * Gets the file name from the user and exports to the file
     *
     * @param actionEvent ActionEvent
     * @throws IOException if the file is not loaded correctly
     */
    @FXML
    private void onExport(ActionEvent actionEvent) throws IOException {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Name Export File");
        dialog.setHeaderText("Give a name to the export file, if the name is taken a new name can be chosen or the existing file can be overwritten.");
        dialog.setContentText("Please enter file name:");
        Optional<String> resultString = dialog.showAndWait();

        if (resultString.isEmpty() || resultString.get().trim().isEmpty()){
            statusLabel.setText("Export unsuccessful");
            return;
        }

        File file = new File(resultString.get().trim() + ".csv");

        if (!file.createNewFile()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Overwrite File?");
            alert.setHeaderText("This file already exists.");
            alert.setContentText("Is it ok to overwrite it?");
            addIconToAlert(alert, logo);

            Optional<ButtonType> resultBoolean = alert.showAndWait();
            if (resultBoolean.isPresent() && resultBoolean.get() != ButtonType.OK) {
                statusLabel.setText("Export unsuccessful");
                return;
            }
        }

        FileManager.exportToFile(file, patientRegistry);
        statusLabel.setText("Export successful");
    }

    /**
     * Opens the about dialog
     *
     * @param actionEvent Event
     */
    @FXML
    private void onAbout(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Hospital Registry");
        alert.setHeaderText("Hospital Registry \nVersion 1.0");
        alert.setContentText("Application by Alida Hjelljord \n(C) 2021.4.29");
        addIconToAlert(alert,logo);

        alert.showAndWait();
    }

    /**
     * No patient selected alert
     */
    private void patientNotFoundAlert() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("No Patient Selected Error");
        alert.setHeaderText("There is no patient selected");
        alert.setContentText("Select the desired patient and try again!");
        addIconToAlert(alert,logo);

        alert.showAndWait();
    }

    /**
     * Adds the logo to the top right corner of alerts
     *
     * @param alert alert to be modified
     * @param image image to be added
     */
    private void addIconToAlert(Alert alert, Image image) {
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(image);
    }
}
