package ntnu.idatt2001.oahjellj.mappe2.hospital;

import javafx.scene.Parent;
import javafx.scene.Scene;
import java.util.Objects;

/**
 * SceneFactory, to fulfill assignment requirements
 *
 * @author OAHjellj
 */
public class SceneFactory {
    public static Scene create(Parent parent) {
        Scene scene = new Scene(parent);

        scene.getStylesheets().add(Objects.requireNonNull(App.class.getResource("style.css")).toExternalForm());
        return scene;
    }
}
