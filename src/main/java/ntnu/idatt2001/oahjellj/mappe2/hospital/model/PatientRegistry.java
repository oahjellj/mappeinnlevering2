package ntnu.idatt2001.oahjellj.mappe2.hospital.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A registry of patients
 *
 * @author OAHjellj
 */
public class PatientRegistry {
    private final List<Patient> patients;

    public PatientRegistry() {
        this.patients = new ArrayList<>();
    }

    /**
     * Constructor that takes a list of patient and add them to the list, ignoring any duplicates
     * @param patients : ArrayList<Patient>
     */
    public PatientRegistry(List<Patient> patients) {
        Objects.requireNonNull(patients, "Patients cannot be null. Use empty constructor instead");

        this.patients = new ArrayList<>();
        addAll(patients);
    }

    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds the given patient to the registry
     * @param patient : Patient
     * @return true if the patient was successfully added
     */
    public boolean add(Patient patient) {
        Objects.requireNonNull(patient, "patient cannot be null");

        if(!patients.contains(patient)) {
            patients.add(patient);
            return true;
        }
        return false;
    }

    /**
     * Adds all patients in the list, ignoring any duplicates
     * @param patients : ArrayList<Patients>
     */
    public void addAll(List<Patient> patients) {
        Objects.requireNonNull(patients, "patient list cannot be null");

        for (Patient patient : patients) {
            add(patient);
        }
    }

    /**
     * Removes the patient from the registry
     * @param patient : Patient
     * @return true if patient was removed
     */
    public boolean remove(Patient patient) {
        return patients.remove(patient);
    }
}

