module hospital {
    requires javafx.controls;
    requires javafx.fxml;

    opens ntnu.idatt2001.oahjellj.mappe2.hospital to javafx.fxml;
    opens ntnu.idatt2001.oahjellj.mappe2.hospital.controller to javafx.fxml;
    opens ntnu.idatt2001.oahjellj.mappe2.hospital.model to javafx.base;

    exports ntnu.idatt2001.oahjellj.mappe2.hospital;
}