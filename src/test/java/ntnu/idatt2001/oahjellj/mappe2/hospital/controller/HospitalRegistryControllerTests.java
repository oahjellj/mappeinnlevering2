package ntnu.idatt2001.oahjellj.mappe2.hospital.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

import ntnu.idatt2001.oahjellj.mappe2.hospital.App;

import ntnu.idatt2001.oahjellj.mappe2.hospital.model.Patient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.framework.junit5.Stop;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

@ExtendWith(ApplicationExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class HospitalRegistryControllerTests {

    @Start
    private void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/HospitalRegistryView.fxml"));
        Parent parent = fxmlLoader.load();
        Scene scene = new Scene(parent);
        stage.setScene(scene);

        stage.show();
    }

    @Stop
    private void stop() throws TimeoutException {
        FxToolkit.cleanupStages();
    }

    @Test
    public void add_adds(FxRobot robot) {
        robot.clickOn("#addPatientButton");
        robot.clickOn("#firstNameTextField").write("Tester");
        robot.clickOn("#lastNameTextField").write("MacTest");
        robot.clickOn("#socialSecurityNumberTextField").write("12345678910");
        robot.clickOn("#saveButton");

        TableView<Patient> patientTableView = robot.lookup("#patientTable").query();
        Assertions.assertTrue(patientTableView.getItems().contains(new Patient("Tester", "MacTest", "", "12345678910")));
    }

    @Test
    public void edit_withNoSelected_alerts(FxRobot robot) {
        robot.clickOn("#editPatientButton");

        Assertions.assertEquals(2, robot.listWindows().size());
    }

    @Test
    public void remove_withNoSelected_alerts(FxRobot robot) {
        robot.clickOn("#removePatientButton");

        Assertions.assertEquals(2, robot.listWindows().size());
    }
}
