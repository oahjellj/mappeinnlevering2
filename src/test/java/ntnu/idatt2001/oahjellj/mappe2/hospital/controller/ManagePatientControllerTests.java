package ntnu.idatt2001.oahjellj.mappe2.hospital.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import ntnu.idatt2001.oahjellj.mappe2.hospital.App;

import ntnu.idatt2001.oahjellj.mappe2.hospital.model.Patient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.framework.junit5.Stop;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.testfx.api.FxAssert.verifyThat;

@ExtendWith(ApplicationExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ManagePatientControllerTests {
    private ManagePatientController managePatientController;

    private String firstNameTextFieldSelector = "#firstNameTextField";
    private String lastNameTextFieldSelector = "#lastNameTextField";
    private String socialSecurityNumberTextFieldSelector = "#socialSecurityNumberTextField";
    private String diagnosisTextFieldSelector = "#diagnosisTextField";
    private String saveButtonSelector = "#saveButton";

    @Start
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("view/ManagePatientView.fxml"));
        Parent parent = fxmlLoader.load();

        managePatientController = fxmlLoader.getController();

        stage.setScene(new Scene(parent));
        stage.show();
    }

    @Stop
    public void stop() throws TimeoutException {
        FxToolkit.cleanupStages();
    }

    @Test
    public void add_withIllegalNameInputs_alerts(FxRobot robot) {
        robot.clickOn(socialSecurityNumberTextFieldSelector).write("12345678901");
        robot.clickOn(saveButtonSelector);

        Assertions.assertEquals(2, robot.listWindows().size());
    }

    @Test
    public void add_withIllegalSocialInputs_alerts(FxRobot robot) {
        robot.clickOn(firstNameTextFieldSelector).write("test");
        robot.clickOn(lastNameTextFieldSelector).write("Test");
        robot.clickOn(saveButtonSelector);

        Assertions.assertEquals(2, robot.listWindows().size());
    }
}
