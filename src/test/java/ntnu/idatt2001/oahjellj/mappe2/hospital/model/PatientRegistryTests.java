package ntnu.idatt2001.oahjellj.mappe2.hospital.model;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PatientRegistryTests {

    @Nested
    public class ConstructorTests {

        @Test
        public void constructor_nullPatients_throws() {
            assertThrows(NullPointerException.class, () -> new PatientRegistry(null));
        }

        @Test
        public void constructor_noDuplicatesInList_addsAll() {
            ArrayList<Patient> patients = new ArrayList<>();
            patients.add(new Patient("Ben","Sin","Matti Ompa","15085507939"));
            patients.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566"));

            PatientRegistry patientRegistry = new PatientRegistry(patients);

            assertEquals(patients, patientRegistry.getPatients());
        }

        @Test
        public void constructor_duplicatesInList_addsNoDuplicates() {
            ArrayList<Patient> patients = new ArrayList<>();
            patients.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566"));
            patients.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566"));

            PatientRegistry patientRegistry = new PatientRegistry(patients);

            assertNotEquals(patients, patientRegistry.getPatients());

            ArrayList<Patient> patientsWithoutDupes = new ArrayList<>();
            patientsWithoutDupes.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566"));

            assertEquals(patientsWithoutDupes, patientRegistry.getPatients());
        }
    }

    @Nested
    public class addTests {

        @Test
        public void add_newPatient_returnsTrue() {
            PatientRegistry patientRegistry = new PatientRegistry();
            Patient patient = new Patient("Billy", "Strøm", "Matti Ompa","31088031566");

            assertTrue(patientRegistry.add(patient));
            assertTrue(patientRegistry.getPatients().contains(patient));
        }

        @Test
        public void add_duplicatePatient_returnsFalse() {
            PatientRegistry patientRegistry = new PatientRegistry();
            patientRegistry.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566"));

            assertFalse(patientRegistry.add(new Patient("Billy", "Strøm", "Matti Ompa","31088031566")));
        }

        @Test
        public void add_null_throws() {
            PatientRegistry patientRegistry = new PatientRegistry();

            assertThrows(NullPointerException.class,() -> patientRegistry.add(null));
        }
    }

    @Nested
    public class RemoveTests {

        @Test
        public void remove_existingPatient_returnsTrue() {
            PatientRegistry patientRegistry = new PatientRegistry();
            Patient patient = new Patient("Billy", "Strøm", "Matti Ompa","31088031566");
            patientRegistry.add(patient);

            assertTrue(patientRegistry.remove(patient));
        }

        @Test
        public void remove_newPatient_returnsFalse() {
            PatientRegistry patientRegistry = new PatientRegistry();
            Patient patient = new Patient("Billy", "Strøm", "Matti Ompa","31088031566");
            patientRegistry.add(patient);

            assertFalse(patientRegistry.remove(new Patient("Ben","Sin","Matti Ompa","15085507939")));
        }
    }
}

