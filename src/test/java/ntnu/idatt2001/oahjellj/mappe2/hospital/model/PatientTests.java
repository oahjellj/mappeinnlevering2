package ntnu.idatt2001.oahjellj.mappe2.hospital.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PatientTests {
    @Nested
    public class EqualsTests {

        @Test
        public void equals_EqualPatients_returnsTrue() {
            Patient patient1 = new Patient("Tester", "Testing", "Tester McTester", "12345678901");
            Patient patient2 = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals(patient1, patient2);
        }

        @Test
        public void equals_UnequalPatients_returnsFalse() {
            Patient patient1 = new Patient("Tester", "Testing", "Tester McTester", "12345678901");
            Patient patient2 = new Patient("Tester", "Testing", "Tester McTester", "12345678961");

            assertNotEquals(patient1, patient2);
        }
    }

    @Nested
    public class ConstructorTests {

        @Test
        public void constructor_withNullFirstName_throws() {
            assertThrows(NullPointerException.class, () -> new Patient(null, "Testing", "Tester McTester", "12345678901"));
        }

        @Test
        public void constructor_withNullLastName_throws() {
            assertThrows(NullPointerException.class, () -> new Patient("Tester", null, "Tester McTester", "12345678901"));
        }

        @Test
        public void constructor_withNullGeneralPractitioner_throws() {
            assertThrows(NullPointerException.class, () -> new Patient("Tester", "Testing", null, "12345678901"));
        }

        @Test
        public void constructor_withNullSocialSecurityNumber_throws() {
            assertThrows(NullPointerException.class, () -> new Patient("Tester", "Testing", "Tester McTester", null));
        }

        @Test
        public void constructor_withWrongLengthSocialSecurityNumber_throws() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("Tester", "Testing", "Tester McTester", "12345"));
        }

        @Test
        public void constructor_withNonNumberSocialSecurityNumber_throws() {
            assertThrows(IllegalArgumentException.class, () -> new Patient("Tester", "Testing", "Tester McTester", "123456789ha"));
        }
    }

    @Nested
    public class GetterTests {
        @Test
        public void getFirstName_getsFirstName() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals("Tester", patient.getFirstName());
        }

        @Test
        public void getLastName_getsLastName() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals("Testing", patient.getLastName());
        }

        @Test
        public void getGeneralPractitioner_getsGeneralPractitioner() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals("Tester McTester", patient.getGeneralPractitioner());
        }

        @Test
        public void getDiagnosis_getsDiagnosis() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals("", patient.getDiagnosis());
        }

        @Test
        public void getSocialSecurityNumber_getsSocialSecurityNumber() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            assertEquals("12345678901", patient.getSocialSecurityNumber());
        }
    }

    @Nested
    public class SetterTests {
        @Test
        public void setFirstName_updatesPatient() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            patient.setFirstName("Testing");

            assertEquals("Testing", patient.getFirstName());
        }

        @Test
        public void setLastName_updatesPatient() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            patient.setLastName("Tester");

            assertEquals("Tester", patient.getLastName());
        }

        @Test
        public void setGeneralPractitioner_updatesPatient() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            patient.setGeneralPractitioner("Testing");

            assertEquals("Testing", patient.getGeneralPractitioner());
        }

        @Test
        public void setDiagnosis_updatesPatient() {
            Patient patient = new Patient("Tester", "Testing", "Tester McTester", "12345678901");

            patient.setDiagnosis("Testing");

            assertEquals("Testing", patient.getDiagnosis());
        }
    }
}
