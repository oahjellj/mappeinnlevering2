# Mappe - Del 2

## Hospital Registry

The task for this assignment was to create a patient registry for a hospital with 
a GUI and persistent data. 

The registry has a tableView showing the patients, and a toolbar. The toolbar 
allows for adding, deleting and editing patients. There is also the option to load
patients from a saved file.